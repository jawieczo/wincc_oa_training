The demo works in a non-distributed manner.
One needs to connect the simulator, start the DIM DNS and set it in DE&N and regenerate the tree.

To make it possible to connect across dist and do the integration excercise, it looks like the following is needed

1) On the central machine start the DIM DNS, and add port-forwarding rule in VirtualBox to tunnel port 2505
2) Open the port forwarding for ports 5000, 50001, 50002, 50003 on the central machine and on subsystem machines
3) On subsystem machines, change the hostname to match the one of real pgysical host (pchret25xx); 
   you do it with the sudo hostname pchretxx
4) make sure that the WinCC OA license is valid for the new hostnames
5) in DEN on subsystem machines, set the new DIM DNS to point to the central machine
6) you need to rename all the CU nodes to be globally unique (eg. to SubDet15-HV, Subdet15-Jura, etc)
7) After that you should be able to integrate the subsystem into the full system