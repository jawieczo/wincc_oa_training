var test_hv_simulator_common_decls_8ctl =
[
    [ "actualState", "test_hv_simulator_common_decls_8ctl.html#a771811623b9e4b8a44b0b13a5820a850", null ],
    [ "actualStateDpeName", "test_hv_simulator_common_decls_8ctl.html#a20a6fcab60a1bb4849beafae58b317be", null ],
    [ "actualVoltage", "test_hv_simulator_common_decls_8ctl.html#af2e8744891bcdc35332467d1bf2b17ed", null ],
    [ "actualVoltageDpeName", "test_hv_simulator_common_decls_8ctl.html#a6ea2ba572ff99fef9809a144325f0d8d", null ],
    [ "adjacentChannelActualState", "test_hv_simulator_common_decls_8ctl.html#a0dbb03373a6aca3127df7f377fa562dc", null ],
    [ "adjacentChannelActualStateDpeName", "test_hv_simulator_common_decls_8ctl.html#aa86e131d06f243060cc2fab4c5abc48c", null ],
    [ "adjacentChannelActualVoltage", "test_hv_simulator_common_decls_8ctl.html#abad7ca7ccba6b235c42545cfb862dd8e", null ],
    [ "adjacentChannelActualVoltageDpeName", "test_hv_simulator_common_decls_8ctl.html#a52e9731a2ab47096ee2dbb91853a4519", null ],
    [ "adjacentChannelName", "test_hv_simulator_common_decls_8ctl.html#a2e59cc833c0175b5b016a0efa0b28c22", null ],
    [ "adjacentChannelPreviousState", "test_hv_simulator_common_decls_8ctl.html#a436cedf3b03c6a3f1ed6c4a1fc0823ed", null ],
    [ "adjacentChannelPreviousVoltage", "test_hv_simulator_common_decls_8ctl.html#a9ca42694afebcc6f23941654515118cc", null ],
    [ "cbTriggerAdjacentChannelDpeName", "test_hv_simulator_common_decls_8ctl.html#a4bb7c590185ded61cef6fdb45f2eb330", null ],
    [ "cbTriggerDpeName", "test_hv_simulator_common_decls_8ctl.html#a771d0144f70134befac3324870657956", null ],
    [ "channelName", "test_hv_simulator_common_decls_8ctl.html#a2ead727b8d5615c9fe9d649865395edf", null ],
    [ "off", "test_hv_simulator_common_decls_8ctl.html#adb67e275e89927c918987f070d8c49fa", null ],
    [ "on", "test_hv_simulator_common_decls_8ctl.html#aaa928c9a62449f7946da1e32f66c70d2", null ],
    [ "previousState", "test_hv_simulator_common_decls_8ctl.html#a00067cf61e21c080fbcde017f10a93cf", null ],
    [ "previousVoltage", "test_hv_simulator_common_decls_8ctl.html#a611d4f83fca3223943e5905fe399ff80", null ]
];