var dir_5919c14f8b789018ba6a976102c66888 =
[
    [ "3.1TableDemo.pnl", "3_81_table_demo_8pnl.html", "3_81_table_demo_8pnl" ],
    [ "aestheticFullHvChannel.pnl", "aesthetic_full_hv_channel_8pnl.html", "aesthetic_full_hv_channel_8pnl" ],
    [ "bubbleCircleYellow.pnl", "bubble_circle_yellow_8pnl.html", "bubble_circle_yellow_8pnl" ],
    [ "canOnlyEnterANumberWithinAGivenRange.pnl", "can_only_enter_a_number_within_a_given_range_8pnl.html", "can_only_enter_a_number_within_a_given_range_8pnl" ],
    [ "canOnlyTypeInAFloat.pnl", "can_only_type_in_a_float_8pnl.html", "can_only_type_in_a_float_8pnl" ],
    [ "colourBlindHvChannel.pnl", "colour_blind_hv_channel_8pnl.html", "colour_blind_hv_channel_8pnl" ],
    [ "demoBarTrendWidgetAsACurve.pnl", "demo_bar_trend_widget_as_a_curve_8pnl.html", "demo_bar_trend_widget_as_a_curve_8pnl" ],
    [ "demonstrateTheDebugger.pnl", "demonstrate_the_debugger_8pnl.html", "demonstrate_the_debugger_8pnl" ],
    [ "dynamicChannelViewer.pnl", "dynamic_channel_viewer_8pnl.html", "dynamic_channel_viewer_8pnl" ],
    [ "exerciseBlob.pnl", "exercise_blob_8pnl.html", "exercise_blob_8pnl" ],
    [ "fullHvChannel.pnl", "full_hv_channel_8pnl.html", "full_hv_channel_8pnl" ],
    [ "gradientColourDemo.pnl", "gradient_colour_demo_8pnl.html", "gradient_colour_demo_8pnl" ],
    [ "hvChannelButtons.pnl", "hv_channel_buttons_8pnl.html", "hv_channel_buttons_8pnl" ],
    [ "hvChannelLeds.pnl", "hv_channel_leds_8pnl.html", "hv_channel_leds_8pnl" ],
    [ "hvChannelName.pnl", "hv_channel_name_8pnl.html", "hv_channel_name_8pnl" ],
    [ "mainBubbles.pnl", "main_bubbles_8pnl.html", "main_bubbles_8pnl" ],
    [ "muonHvChannels.pnl", "muon_hv_channels_8pnl.html", "muon_hv_channels_8pnl" ],
    [ "otherButtonPossibilities.pnl", "other_button_possibilities_8pnl.html", "other_button_possibilities_8pnl" ],
    [ "rgb.pnl", "rgb_8pnl.html", "rgb_8pnl" ],
    [ "showEvents.pnl", "show_events_8pnl.html", "show_events_8pnl" ],
    [ "sineDemo.pnl", "sine_demo_8pnl.html", "sine_demo_8pnl" ],
    [ "spinner.pnl", "spinner_8pnl.html", "spinner_8pnl" ],
    [ "spinner8.pnl", "spinner8_8pnl.html", "spinner8_8pnl" ],
    [ "syncTypesInfo.pnl", "sync_types_info_8pnl.html", "sync_types_info_8pnl" ],
    [ "versionDisplay.pnl", "version_display_8pnl.html", "version_display_8pnl" ]
];