// <proj_path>/scripts/libs/Solutions/solutions_hvSimulatorLibDebugged.ctl
/**@file

This library contains the code for the hvSimulator as demonstrated in the PVSS Course. 
The code is intended to be run in the background in a CTRL manager. It simulates voltage ramping on an hvChannel.

This code is the debugged version of the simulator. (ie I believe it works ;-)

@par Creation Date
	30/5/2014

@par Modification History
    Original version
    
@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	CTRL

@author
	Paul Burkimsher EN-ICE
*/

// -----------------------------------------------------------------

/** 
@par Purpose: 
Simulate the voltage change on a channel (DPE actual.vMon) when he clicks the channel On or Off (DPE settings.onOff).
             

@par Constraints
	This is a callback routine(CB) and is not called explicitly. This callback (CB) routine gets called whenever <someChannel>.settings.onOff
              (i.e. requested state) changes.

@param dpeThatChanged   input, The name of the DPE that changed
@param value			input, The value the above DPE just changed to

*/
void solutions_hvSimulatorCB(string dpeThatChanged, bool value)
{

    string dpeName; // Local variable for constructing dpe name of interest.

    int replyCode;  // Functions' return values should always be checked!

    unsigned numberOfSteps; // How many intermediate steps in the ramping process
        
    float totalVoltageDelta, // Total voltage change from start to finish
            voltageStepSize,   // Voltage change at each step
            targetVoltage;     // The voltage we're aiming for

    const float rampRate = 2.0; // The ramp *rate* in V/sec (might be ramping up or down).
          
    float v0Var,   // Local variable version of the v0 dpe's value.
          vMonVar; // Local variable version of the vMon dpe's value.

    voltageStepSize = 1.0; // Defau1t value... 


// Section 1.
// This simulator is simulating the voltage change.
// We need to know the initial voltage and the target voltage.

// These are held in the <channel>actual.vMon and <channel>settings.v0 respectively.
// But we are in a CTRL manager, whereas settings.v0 and actual.vMon 
// are DPEs in the Event Manager.
// We get them into the CTRL Mgr by asking the Event Manager. We use the dpGet() function.
// NB WinCC-OA functions are functions which return a status code!

// Construct the name of the dpe we wish to get.
// Extract the system name and root of the datapoint (DP) name from our dpe input parameter
// and append onto it the branch names down to the DPE 
    dpeName = dpSubStr(dpeThatChanged,DPSUB_SYS_DP) + ".settings.v0";
    // and get that DPE's value into a local variable.
    replyCode = dpGet(dpeName,v0Var);
    if ( replyCode != 0 ) 
       { DebugTN("1: hvSimulator.ctl: Error=" + replyCode + " from dpGet(" + dpeName + ")" );
       }

// Simlarly get vMon...
    dpeName = dpSubStr(dpeThatChanged,DPSUB_SYS_DP) + ".actual.vMon";
    replyCode = dpGet(dpeName,vMonVar); 
    if ( replyCode != 0 ) 
       { DebugTN("2: hvSimulator.ctl: Error=" + replyCode + " from dpGet(" + dpeName + ")" );
       }

    targetVoltage = 10.0; // Default value...

// Remember: We have just been triggered because muonHvChannel1.settings.onOff was changed.
// We have to move the actual voltage (vMon) to the target voltage (v0).
    targetVoltage = v0Var;
    // Unless he's just pressed Switch Off of course,
    // in which case we head to zero volts and ignore the v0 value...
    if ( 0 == value ) { targetVoltage = 0.0; } 

    // How many volts must we ramp in total?
    totalVoltageDelta = targetVoltage - vMonVar; // Might be negative, positive or even zero...
         
    try { // How many steps (times round the loop) must we go...
              numberOfSteps = (unsigned) fabs( totalVoltageDelta / rampRate );
        }
    catch
        { dyn_errClass exceptionIJustCaught = getLastException();
          DebugTN("Caught this exception ",exceptionIJustCaught);
          DebugTN("When we were computing numberOfSteps = totalVoltageDelta / rampRate with");
          DebugTN("totalVoltageDelta = ",totalVoltageDelta);
          DebugTN("rampRate = ",rampRate);
          // Now what? If we re-raise the exception, we'll get an error at run-time.
          // Re-raise like this:
          // throw(exceptionIJustCaught[1]);
          // But we don't want it to crash at run-time
          // Instead let's cause the "ramp" to just do nothing. (We've already put lots of messages in the log!)
          DebugTN("Therefore we choose to IGNORE this attempt to ramp the voltage!");
          numberOfSteps = (unsigned) 0;  
        }

    try {	// What size must we use for each voltage step?
             voltageStepSize = fabs( totalVoltageDelta / numberOfSteps );
        }
    catch
        { // NB By here, the exception has been caught and cleared, so getLastError() shows nothing. 
          dyn_errClass exceptionIJustCaught = getLastException();
          DebugTN("Caught this exception ",exceptionIJustCaught);
          DebugTN("When we were computing voltageStepSize = totalVoltageDelta / numberOfSteps with");
          DebugTN("totalVoltageDelta = ",totalVoltageDelta);
          DebugTN("numberOfSteps = ",numberOfSteps);      
          // Now what? If we re-raise the exception, we'll get an error at run-time.
          // Re-raise like this:
          // throw(exceptionIJustCaught[1]);
          // But we don't want it to crash at run-time
          // Instead let's cause the "ramp" to just do nothing. (We've already put lots of messages in the log!)
          DebugTN("Therefore we choose to IGNORE this attempt to ramp the voltage!");
          numberOfSteps = (unsigned) 0;  
          voltageStepSize = 1.0; // Anything really, as it won't actually be used.
        }
    
// What is the name of the DPE where we place the simulated voltage?	 
    dpeName = dpSubStr(dpeThatChanged,DPSUB_SYS_DP) + ".actual.vMon";

    // Ramp it!
    _solutions_rampVoltage(dpeName, vMonVar, targetVoltage, numberOfSteps, voltageStepSize);
      
    // Simulate the new state (On or Off)
       dpeName = dpSubStr(dpeThatChanged,DPSUB_SYS_DP) + ".actual.isOn";
       replyCode = dpSetWait( dpeName, value ); // value was given to us as a parameter when settings.onOff changed
       if ( replyCode != 0 ) 
         { DebugTN("3: hvSimulator.ctl: Error=" + replyCode + " from dpSetWait(" + dpeName + ")" );
         }

    // Make the screen pretty (i.e. hide any rounding errors!)
       dpeName = dpSubStr(dpeThatChanged,DPSUB_SYS_DP) + ".actual.vMon";
       replyCode = dpSetWait(dpeName, targetVoltage);   
       if ( replyCode != 0 ) 
         { DebugTN("4: hvSimulator.ctl: Error=" + replyCode + " from dpSetWait(" + dpeName + ")" );
         }

} // END hvSimulatorCB

// ----------------------------------------------------------------------------

/**
@par Purpose: 
To move the vMon (named in dpeName) from its initial value to its target value
         in 'numberOfSteps' steps,
         of 'voltageStepSize' each,
         at the rate of one step per second.
@par Contraints: 
Private routine.
@param dpeName         input, The name of the dpe to ramp. (Is always *.actual.Vmon)
@param initialVMon      input, The voltage to start ramping from
@param targetVMon       input, The voltage to ramp to
@param numberOfSteps    input, How many steps to take over the ramping
@param voltageStepSize  input, The voltage increment that should be applied at each step.
@par Side effects:
The dpe identified in dpeName has its value updated.
@par Remarks
One step per second is hard-coded.
*/
private void _solutions_rampVoltage(
                string dpeName, 
                float initialVMon, float targetVMon, 
                unsigned numberOfSteps, float voltageStepSize)
{
     float voltageStep; // Change in voltage each time around the loop.
     int step,          // Loop counter.
           replyCode;     // Functions' return values should be checked!
     float vMonVar = initialVMon; // Local VARiable. Corresponds to current value inside actual.vMon

     /*
     DebugTN("Entering rampVoltage with initialVmon = " + initialVMon + 
            "; targetVMon = " + targetVMon +
            "; numberOfSteps = " + numberOfSteps +
            "; voltageStepSize = " + voltageStepSize
           );
     */

     // Be sure whether we are adding or subtracting the voltage step!
     voltageStep = fabs(voltageStepSize);
     if ( targetVMon < initialVMon ) voltageStep = -voltageStep;
      
     for (step = 1 ; step <= numberOfSteps ; step++ )
        {
          vMonVar = vMonVar + voltageStep; // voltageStep might be negative.
          // Set the new value in the DP so that it appears on the screen.
          replyCode = dpSetWait(dpeName,vMonVar);
          if ( replyCode != 0 ) 
            { 
             DebugTN("5: hvSimulator.ctl: Error=" + replyCode + " from dpSetWait(" + dpeName + ")" );
            }
          delay( 1 ); // sec
         }  

} // END rampVoltage



