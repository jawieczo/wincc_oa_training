/**@file <proj_path>/scripts/libs/Test/Solutions/testHvSimulatorCommonDecls.ctl  
  
@brief Common declarations used by all of the routines in hvSimulator.ctl library. 
  
@par Creation Date
         30/05/2014   
   
 */

// It would be nice to just write these declarations as global variables in the hvSimulator.ctl file itself.
// Unfortunately, if we do that, then they are out of scope when executed by the "eval" in the UnitTestHarness.
// Putting them in a little library of their own a(ie here) nd #use-ing this library makes them visible again.
// PCB May 2014

// The following declarations are common across the test setup routines and the test case routines in the testHvSimulator.ctl file:
    string channelName = "muonHvChannel1";
    string adjacentChannelName = "muonHvChannel2";
    
    string cbTriggerDpeName = channelName + ".settings.onOff";
    string cbTriggerAdjacentChannelDpeName = adjacentChannelName + ".settings.onOff";

    string actualStateDpeName = channelName + ".actual.isOn";
    string adjacentChannelActualStateDpeName = adjacentChannelName + ".actual.isOn";    
    bool previousState, actualState;
    bool adjacentChannelPreviousState,adjacentChannelActualState;
    
    string actualVoltageDpeName = channelName + ".actual.vMon";
    string adjacentChannelActualVoltageDpeName = adjacentChannelName + ".actual.vMon";    
    float previousVoltage, actualVoltage;
    float adjacentChannelPreviousVoltage, adjacentChannelActualVoltage;
    
    bool off = false;
    bool on = true;
    
