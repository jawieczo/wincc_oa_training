/**@file <proj_path>/scripts/libs/Test/Solutions/testHvSimulator.ctl  
  
@brief Test routines for  hvSimulator.ctl library of this component. 
  
@par Creation Date
         16/05/2014   
   
 */
 
// You must pick up this library:
#uses "fwUnitTestComponentAsserts.ctl"

// and, of course, the library of routines you are wanting to call (ie wanting to test):
// From <proj_path>/scripts/libs
#uses "Solutions/solutions_hvSimulatorLib.ctl"

// Pick up the common declarations used across the test_setup routines and the test_case routines in the current file
// (You can't just give them as "global" variables here, see the comment in testHvSimulatorCommonDecls.ctl)
#uses "Test/Solutions/testHvSimulatorCommonDecls.ctl"

// The following declarations are common across the test setup routines and the test case routines in this file:
//    string channelName = "muonHvChannel1";
//    string adjacentChannelName = "muonHvChannel2";
//    
//    string cbTriggerDpeName = channelName + ".settings.onOff";
//    string cbTriggerAdjacentChannelDpeName = adjacentChannelName + ".settings.onOff";
//
//    string actualStateDpeName = channelName + ".actual.isOn";
//    string adjacentChannelActualStateDpeName = adjacentChannelName + ".actual.isOn";    
//    bool previousState, actualState;
//    bool adjacentChannelPreviousState,adjacentChannelActualState;
//    
//    string actualVoltageDpeName = channelName + ".actual.vMon";
//    string adjacentChannelActualVoltageDpeName = adjacentChannelName + ".actual.vMon";    
//    float previousVoltage, actualVoltage;
//    float adjacentChannelPreviousVoltage, adjacentChannelActualVoltage;
//    
//    bool off = false;
//    bool on = true;


//-------------------------------------------------------

/**
@brief A test routine, calls the routine being tested. 
*/ 
// The test case's routine name shall always start with test*
// The name of the routine being tested here, follows the Fw guidelines: LibName_routineName 
// so we call this routine that does the testing testExampleLibToBeTested_sub:

void testSolutions_hvSimulatorCB()
{ 
    // We want to test the simulator routine.
    // The simulator callback is normally invoked whenever
    // an HV channel is requested to switch between states (on or off).
    
    // We assume (see the test case setup routine!) that the channel is currently OFF!!!
    bool requestedState = true; // On
    float requestedVoltage = 100.0;
    int replyCode;
    dyn_errClass err;

    DebugTN("Entering testSolutions_hvSimulatorCB()");
     
    // Get the other conditions prior to calling the simulator
    DebugTN("1actualStateDpeName = ", actualStateDpeName);
    replyCode = dpGet(actualStateDpeName, previousState);
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpGet(" + actualStateDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }
    DebugTN("previousState = ",previousState);

    replyCode = dpGet(adjacentChannelActualStateDpeName, adjacentChannelPreviousState);   
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpGet(" + cbTriggerAdjacentChannelDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }  
    DebugTN("adjacentChannelPreviousState = ",adjacentChannelPreviousState);
    
    
    
    replyCode = dpGet(actualVoltageDpeName, previousVoltage);
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpGet(" + actualVoltageDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }
    DebugTN("previousVoltage = ",previousVoltage);
    
    replyCode = dpGet(adjacentChannelActualVoltageDpeName, adjacentChannelPreviousVoltage);      
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpGet(" + adjacentChannelActualVoltageDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }    
    DebugTN("adjacentChannelPreviousVoltage = ",adjacentChannelPreviousVoltage);    
   
    
    
    
    
    
    // Call the simulator
    DebugTN("About to call the hvSimulator as if:");
    DebugTN("cbTriggerDpeName = ", cbTriggerDpeName);    
    DebugTN("requestedState = ",requestedState);
    replyCode = dpSetWait(cbTriggerDpeName, requestedState);
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpSetWait(" + cbTriggerDpeName + "," + requestedState +") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }    
 
    solutions_hvSimulatorCB(cbTriggerDpeName,  requestedState);
      
    
    
    
    
    // Verify that after the ramping has completed (ie when the simulation is over) the conditions are as expected:

    // Verify that the actual.isOn state is as expected:
    replyCode = dpGet(actualStateDpeName, actualState);  
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpGet(" + actualStateDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       } 
    DebugTN("About to assert1 that requestedState (" + requestedState + ") == actualState(" + actualState + ")"    );
    assertEqual(requestedState,actualState, channelName + " requested state and actual state are not the same");
     
    // Verify that the actual.vMon is as expected:
    replyCode = dpGet(actualVoltageDpeName, actualVoltage);  
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpGet(" + actualVoltageDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       } 
    DebugTN("About to assert2 that requestedVoltage (" + requestedVoltage + ") == actualVoltage(" + actualVoltage + ")"    );
    assertEqual(requestedVoltage, actualVoltage, channelName + " requested voltage and actual voltage are not the same");   
    
    // Verify that we have been changing only the correct channel and have not affected its neighbours!
    // Verify that the actual.isOn state of an adjacent channel has not changed
    replyCode = dpGet(adjacentChannelActualStateDpeName, adjacentChannelActualState);   
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpGet(" + adjacentChannelActualStateDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }
    DebugTN("About to assert3 that adjacentChannelPreviousState (" + adjacentChannelPreviousState + ") == adjacentChannelActualState(" + adjacentChannelActualState + ")"    );    
    assertEqual(adjacentChannelPreviousState,adjacentChannelActualState); 
    
    // Verify that the actual.vMon of an adjacent channel has not changed
    replyCode = dpGet(adjacentChannelActualVoltageDpeName, adjacentChannelActualVoltage);      
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl : Error:  dpGet(" + adjacentChannelActualVoltageDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }
    DebugTN("About to assert4 that adjacentChannelPreviousVoltage (" + adjacentChannelPreviousVoltage + ") == adjacentChannelActualVoltage(" + adjacentChannelActualVoltage + ")"    );      
    assertEqual(adjacentChannelPreviousVoltage, adjacentChannelActualVoltage);    
  
    DebugTN("Exiting testSolutions_hvSimulatorCB()");  

} // END testSolutions_hvSimulatorCB()

//-------------------------------------------------------

/**
@brief This routine is optional.
Prepare the appropriate global environment for all of the test cases in this suite.
setupSuite() is called once only, before any of the test cases are called.
*/ 
void testSolutions_setupSuite()
{
	DebugTN("Entering testSolutions_setupSuite()");
    DebugTN("(Nothing to do.)");
	DebugTN("Exiting testSolutions_setupSuite()");
}

//-------------------------------------------------------

/**
@brief testSolutions_setup() ensures a consistent environment exists prior to calling each test case.
This routine is optional. If you declare one here, it should prepare any appropriate environment
that will be required before running each of the test cases in this suite.

*/ 
void testSolutions_setup()
{ 
    int replyCode;
    dyn_errClass err;
	  DebugTN("Entering testSolutions_setup()");
    // Set the conditions prior to calling the simulator test
    // Before each test we ensure that the channel of interest is OFF:

    int initialVoltage;

    DebugTN("2actualStateDpeName = " ,actualStateDpeName);
    
    replyCode = dpSetWait(actualStateDpeName, off);
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl/testSolutions_setup() : Error:  dpSetWait(" + actualStateDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }
    
    // and that the actual voltage is set to a well defined value
    initialVoltage = 44.0; // Anything really.
    replyCode = dpSetWait(actualVoltageDpeName ,  initialVoltage);
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl/testSolutions_setup() : Error:  dpSetWait(" + actualVoltageDpeName + " = " + initialVoltage + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }  

    
    // Ditto for the adjacent channel:
    replyCode = dpSetWait(adjacentChannelActualStateDpeName, off);
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl/testSolutions_setup() : Error:  dpSetWait(" + adjacentChannelActualStateDpeName + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }
    
    // and that the actual voltage is set to a well defined value
    initialVoltage = 45.0; // Anything distinguishable from the other channel really.
    replyCode = dpSetWait(adjacentChannelActualVoltageDpeName ,  initialVoltage);
    err = getLastError();
    if(dynlen(err) > 0) 
       { DebugTN("testHvSimulator.ctl/testSolutions_setup() : Error:  dpSetWait(" + adjacentChannelActualVoltageDpeName + " = " + initialVoltage + ") failed");
         DebugTN("err = ",err);
         throwError(err); // write errors to stderr 
       }    
    
	  DebugTN("Exiting testSolutions_setup()");    
    
    
    
} // END

