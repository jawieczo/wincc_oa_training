// <proj_path>/scripts/Solutions/hvSimulator.ctl
// Purpose: Respond when buttons etc. are pressed on the HV panel.
//          Simulate voltage ramping.

// This code calls a library which embodies some bugs. Your job is to understand the code,
// watch it break and then debug it :-)

// Where are the bugs? In the library. (But it would be cheating if I told you any more than that...)
// Clue: Try to switch an HV Channel off when it is already off.
//       Watch the logfile for errors ;-)

#uses "Solutions/solutions_hvSimulatorLib.ctl"

void main()
{ int replyCode;
  string dpeName;

  // dpConnect() our 3 HV channels, each to its own simulator code instance, running in its own thread.
  // When you have completed the basic exercise, you can look at the extension to this exercise
  //  for a more intelligent, and more general, piece of code to do this!
  
   dpeName = "muonHvChannel1.settings.onOff";
   replyCode = dpConnect ("solutions_hvSimulatorCB", FALSE, dpeName );
   if ( replyCode != 0 )
      { DebugTN("hvSimulator.ctl: Error=" + replyCode + " from dpConnect() to dpeName = " + dpeName );
      }
   
   dpeName = "muonHvChannel2.settings.onOff";
   replyCode = dpConnect ("solutions_hvSimulatorCB", FALSE, dpeName );
   
   if ( replyCode != 0 )
      { DebugTN("hvSimulator.ctl: Error=" + replyCode + " from dpConnect() to dpeName = " + dpeName );
      }
   
   dpeName = "muonHvChannel3.settings.onOff";
   replyCode = dpConnect ("solutions_hvSimulatorCB", FALSE, dpeName );
   if ( replyCode != 0 )
      { DebugTN("hvSimulator.ctl: Error=" + replyCode + " from dpConnect() to dpeName = " + dpeName );
      }

} // END main


