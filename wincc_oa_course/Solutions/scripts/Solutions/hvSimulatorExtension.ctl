// <proj_path>/scripts/Solutions/hvSimulatorExtension.ctl

// -----------------------------------------------------------------
// 
// A fragment of code that you can use, when you wish to simulate more than just a few channels.
// It shows how, in the general case, to dpConnect()  to ALL DPs of type FcHvChannelType.
// It dpConnect()s to all the DPs whose names match "*" and are of type FcHvChannelType

// Use this code as your simulator's "main()" program, instead of the original main() that 
// connected to only 3 channels explicitly by name.

#uses "Solutions/solutions_hvSimulatorLib.ctl"

void main()
{
    dyn_string hvChannels; // Declare an array of strings.
    int channelIndex;
    int replyCode;
    string dpeName;
    
    // Populate the array with a list of all our channels, ie with all of the DP names of 
    // DPtype "FcHvChannelType".
    // (See the write up of dpNames in the Online Help.)
    hvChannels = dpNames("*", "FcHvChannelType");
    
    DebugTN("Found these hvChannels: ",hvChannels); // For debugging, we send the list of DP names
                                                    // that we found to the log file.
    
    // Now, one channel at a time, dpConnect the simulator routine to 
    // the appropriate DPE (located inside each channel DP).
    for( channelIndex = 1;  channelIndex <= dynlen( hvChannels );  channelIndex++)
        { dpeName = hvChannels[channelIndex] + ".settings.onOff"; 
          replyCode = dpConnect("solutions_hvSimulatorCB", FALSE, dpeName);
          if ( replyCode != 0 )
             { DebugTN("hvSimulatorExtension.ctl: Error=" + replyCode + " from dpConnect() to dpeName = " + dpeName );
             }
        }

} // END main

